#Notes

##Копирование репозитория
- https://gitlab.com/i6n-support/i6n-notification

- Создать папку -> git bash here

- clone with SSH

- `git clone git@gitlab.com:i6n-support/i6n-notification.git`

##Запуск Docker Compose
- `docker-compose up` в терминале

##Первый запуск

- edit configurations 
- active profiles: `local`

##Hotkeys

- `win + F6` - Поменять имя везде
- `double Shift` - Поиск по файлам
- `Ctrl + Shift + F` - поиск подстроки по проекту
- `Ctrl + Shift + R` - замена подстроки по проекту 
- `Ctrl + P` - Посмотреть принимаемые аргументы