FROM adoptopenjdk/openjdk11:alpine-jre

ENV SERVICE_NAME=notification

ENV SPRING_DATASOURCE_URL=jdbc:postgresql://185.246.65.69:5432/notification
ENV SPRING_DATASOURCE_USERNAME=slimecode
ENV SPRING_DATASOURCE_PASSWORD=F4yBCe8Hh7LQJGYG

ENV SPRING_KAFKA_BOOTSTRAP-SERVERS: kafka:19092

ADD ${SERVICE_NAME}-service-src/target/${SERVICE_NAME}-service-src-0.0.1-SNAPSHOT.jar /app.jar

ENTRYPOINT exec java -jar /app.jar