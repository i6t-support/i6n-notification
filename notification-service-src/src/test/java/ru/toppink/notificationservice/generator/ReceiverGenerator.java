package ru.toppink.notificationservice.generator;

import ru.toppink.notificationservice.model.Notification;
import ru.toppink.notificationservice.model.Receiver;

public class ReceiverGenerator {

    public static Receiver getReceiver(Notification notification, String login) {
        Receiver receiver = new Receiver();
        receiver.setLogin(login);
        receiver.setNotification(notification);
        receiver.setRead(false);
        return receiver;
    }
}
