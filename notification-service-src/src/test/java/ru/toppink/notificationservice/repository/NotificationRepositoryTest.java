/*
package ru.toppink.notificationservice.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;
import ru.toppink.notificationservice.common.DataBaseTest;
import ru.toppink.notificationservice.generator.ReceiverGenerator;
import ru.toppink.notificationservice.model.Notification;
import ru.toppink.notificationservice.model.NotificationType;
import ru.toppink.notificationservice.model.Receiver;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Sql({
        "classpath:scripts/insert-notifications.sql",
        "classpath:scripts/insert-receivers.sql"
})
@Transactional
public class NotificationRepositoryTest extends DataBaseTest {

    @MockBean
    private KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    private NotificationRepository repository;

    @Test
    public void shouldSaveNotification() {
        String applicationId = UUID.randomUUID().toString();
        String executor = "p.petrov";
        NotificationType type = NotificationType.NEW_TASK;

        Set<Receiver> receivers = new HashSet<>();
        Notification saveable = new Notification(
                UUID.randomUUID(), applicationId, executor, OffsetDateTime.now(), type, receivers
        );

        receivers.add(ReceiverGenerator.getReceiver(saveable, "login1"));
        receivers.add(ReceiverGenerator.getReceiver(saveable, "login2"));

        Notification saved = repository.save(saveable);
        assertNotNull(saved);
    }

    @Test
    public void shouldGetNotificationById() {
        Notification notification = repository.findById(UUID.fromString("e4ccfd87-2dce-40c6-a149-2b5b9c6602d1")).get();

        Receiver receiver1 = Receiver.builder()
                .id(UUID.fromString("aa8a0779-ecc3-427e-8b0a-ba64c28a05c2"))
                .notification(notification)
                .login("login")
                .isRead(true)
                .build();

        Receiver receiver2 = Receiver.builder()
                .id(UUID.fromString("31fc9e72-948e-47d8-a845-033149a12289"))
                .notification(notification)
                .login("login3")
                .isRead(false)
                .build();

        assertNotNull(notification);
        assertEquals("e4ccfd87-2dce-40c6-a149-2b5b9c6602d1", notification.getId().toString());
        assertEquals("41d7d7c4-e5db-422b-940f-fc2b43d377d7", notification.getApplicationId());
        assertEquals("login2", notification.getExecutor());
       //assertEquals(
       //        OffsetDateTime.parse("2021-04-18T17:17:23.555555Z"),
       //        notification.getCreatedAt()
       //);
        assertEquals(NotificationType.NEW_TASK, notification.getType());
        assertTrue(notification.getReceivers().contains(receiver1));
        assertTrue(notification.getReceivers().contains(receiver2));
        assertEquals(2, notification.getReceivers().size());
    }

    @Test
    public void shouldGetNotificationByLogin(){
        Page<Notification> savedPage = repository.getNotificationByLogin("login2", PageRequest.of(0, 10));

        String id1 = "5cbad178-fb92-49cb-b2fd-6af2e0634da1";
        String id2 = "e944e3b0-f011-4ce8-b9b3-594d76daa71d";

        assertNotNull(savedPage);
        assertEquals(id1, savedPage.getContent().get(0).getId().toString());
        assertEquals(id2, savedPage.getContent().get(1).getId().toString());
        assertEquals(2, savedPage.getContent().size());
    }
}
*/
