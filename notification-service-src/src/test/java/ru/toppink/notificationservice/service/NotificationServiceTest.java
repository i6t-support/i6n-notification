/*
package ru.toppink.notificationservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.notificationservice.AbstractTest;
import ru.toppink.notificationservice.dto.NotificationDto;
import ru.toppink.notificationservice.generator.ReceiverGenerator;
import ru.toppink.notificationservice.model.Notification;
import ru.toppink.notificationservice.model.NotificationType;
import ru.toppink.notificationservice.model.Receiver;
import ru.toppink.notificationservice.repository.NotificationRepository;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = NotificationServiceImpl.class)
public class NotificationServiceTest extends AbstractTest {

    @Autowired
    private NotificationService notificationService;

    @MockBean
    private NotificationRepository notificationRepository;

    @MockBean
    private SecurityService securityService;

    @Test
    public void shouldCreateNotification() {
        String applicationId = UUID.randomUUID().toString();
        String executor = "p.petrov";
        NotificationType type = NotificationType.NEW_TASK;

        Set<String> receiverLogins = Set.of("login1", "login2");

        Set<Receiver> receivers = new HashSet<>();
        Notification mockAnswerForSave = new Notification(
                UUID.randomUUID(), applicationId, executor, OffsetDateTime.now(), type, receivers
        );
        receivers.add(ReceiverGenerator.getReceiver(mockAnswerForSave, "login1"));
        receivers.add(ReceiverGenerator.getReceiver(mockAnswerForSave, "login2"));

        when(notificationRepository.save(any(Notification.class)))
                .thenReturn(mockAnswerForSave);

        notificationService.create(applicationId, executor, type, receiverLogins);

        verify(notificationRepository, times(1)).save(
                argThat(saved -> {
                    assertNotNull(saved);
                    assertEquals(applicationId, saved.getApplicationId());
                    assertEquals(executor, saved.getExecutor());
                    assertEquals(type, saved.getType());
                    assertEquals(receivers, saved.getReceivers());
                    assertNotNull(saved.getCreatedAt());
                    return true;
                })
        );
    }

    @Test
    public void shouldGetNotificationsByLogin(){
        String login = "login";

        Notification mockNotification = new Notification(
                UUID.randomUUID(), UUID.randomUUID().toString(), "chel", OffsetDateTime.now(),
                NotificationType.CHANGE_STATUS, Set.of(ReceiverGenerator.getReceiver(null, "chel"))
        );

        Page<Notification> mockPageNotification = new PageImpl<>(List.of(mockNotification));


        when(notificationRepository.getNotificationByLogin(eq(login), any(Pageable.class)))
                .thenReturn(mockPageNotification);

        Page<NotificationDto> notifications = notificationService.getByLogin(login, Pageable.unpaged());
        NotificationDto actualNotification = notifications.getContent().get(0);

        assertNotNull(notifications);
        assertEquals(1, notifications.getContent().size());
        assertEquals(mockNotification.getApplicationId(), actualNotification.getApplicationId());
        assertEquals(mockNotification.getId(), actualNotification.getId());
        assertEquals(mockNotification.getCreatedAt(), actualNotification.getCreatedAt());
        assertEquals(mockNotification.getExecutor(), actualNotification.getExecutor());
        assertEquals(mockNotification.getType(), actualNotification.getType());
    }

}
*/
