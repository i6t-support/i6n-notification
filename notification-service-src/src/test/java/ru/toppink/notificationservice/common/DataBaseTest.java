package ru.toppink.notificationservice.common;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.toppink.notificationservice.AbstractTest;
import ru.toppink.notificationservice.Application;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(initializers = PostgresInitializer.class)
@SpringBootTest(
        webEnvironment = RANDOM_PORT,
        classes = Application.class
)
public abstract class DataBaseTest extends AbstractTest {

}
