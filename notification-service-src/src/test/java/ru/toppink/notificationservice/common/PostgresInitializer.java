package ru.toppink.notificationservice.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

@Slf4j
public class PostgresInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static final String DOCKER_IMAGE = "postgres:11-alpine";

    public static final PostgreSQLContainer dbContainer = (PostgreSQLContainer) new PostgreSQLContainer(
            DockerImageName.parse(DOCKER_IMAGE).asCompatibleSubstituteFor("postgres")
    )
            .withDatabaseName("template")
            .withUsername("template")
            .withPassword("template")
            .withReuse(true);

    static {
        dbContainer.start();
    }

    @Override
    public void initialize(final ConfigurableApplicationContext configurableApplicationContext) {
        final String jdbcUrl = dbContainer.getJdbcUrl();
        log.debug("Jdbc url: " + jdbcUrl);
        TestPropertyValues.of(
                "spring.datasource.url=" + dbContainer.getJdbcUrl(),
                "spring.datasource.username=" + dbContainer.getUsername(),
                "spring.datasource.password=" + dbContainer.getPassword()
        ).applyTo(configurableApplicationContext.getEnvironment());
    }
}