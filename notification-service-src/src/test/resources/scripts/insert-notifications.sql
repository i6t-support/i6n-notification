SET timezone = 'UTC';

insert into notification(id, application_id, executor, created_at, type)
values ('e944e3b0-f011-4ce8-b9b3-594d76daa71d', '41d7d7c4-e5db-422b-940f-fc2b43d377d7',
        'login', timezone('UTC','2021-04-18T17:12:10.555555'), 'NEW_TASK'
	   );

insert into notification(id, application_id, executor, created_at, type)
values ('e4ccfd87-2dce-40c6-a149-2b5b9c6602d1', '41d7d7c4-e5db-422b-940f-fc2b43d377d7',
		'login2', timezone('UTC','2021-04-18T17:17:23.555555'), 'NEW_TASK'
	   );

insert into notification(id, application_id, executor, created_at, type)
values ('5cbad178-fb92-49cb-b2fd-6af2e0634da1', '08b3ae6a-fc27-4d18-ab46-a283f5c9c2de',
		'login', timezone('UTC','2021-04-18T17:25:10.555555'), 'CHANGE_STATUS'
	   );