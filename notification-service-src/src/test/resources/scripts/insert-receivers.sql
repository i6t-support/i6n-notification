insert into receiver(id, notification_id, login, is_read)
VALUES ('aa8a0779-ecc3-427e-8b0a-ba64c28a05c2', 'e4ccfd87-2dce-40c6-a149-2b5b9c6602d1',
        'login', true);

insert into receiver(id, notification_id, login, is_read)
VALUES ('31fc9e72-948e-47d8-a845-033149a12289', 'e4ccfd87-2dce-40c6-a149-2b5b9c6602d1',
		'login3', false);

insert into receiver(id, notification_id, login, is_read)
VALUES ('5e045c24-25ee-4123-a658-025998e27755', '5cbad178-fb92-49cb-b2fd-6af2e0634da1',
		'login2', false);

insert into receiver(id, notification_id, login, is_read)
VALUES ('68a5b1b9-ed38-43f8-8c28-66850b988be5', 'e944e3b0-f011-4ce8-b9b3-594d76daa71d',
		'login2', true);

