package ru.toppink.notificationservice.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Service;
import ru.toppink.notificationservice.model.NotificationType;
import ru.toppink.notificationservice.service.NotificationService;
import ru.toppink.userservice.api.event.UserAuthorizeEvent;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserAuthorizeListener implements ApplicationListener<PayloadApplicationEvent<UserAuthorizeEvent>> {

    private final NotificationService notificationService;

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<UserAuthorizeEvent> userAuthorizeEventPayloadApplicationEvent) {
        UserAuthorizeEvent event = userAuthorizeEventPayloadApplicationEvent.getPayload();
        notificationService.create(
                null,
                event.getLogin(),
                NotificationType.AUTHORIZE,
                Set.of(event.getLogin())
        );
    }
}
