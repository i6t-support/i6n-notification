package ru.toppink.notificationservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification")
public class Notification {

    @Id
    @GeneratedValue
    private UUID id;
    private String applicationId;
    private String executor;
    private OffsetDateTime createdAt;
    @Enumerated(value = EnumType.STRING)
    private NotificationType type;
    @OneToMany(mappedBy = "notification", fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    private Set<Receiver> receivers;

}
