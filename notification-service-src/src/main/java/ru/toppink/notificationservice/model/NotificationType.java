package ru.toppink.notificationservice.model;

public enum NotificationType {

    AUTHORIZE,
    NEW_TASK,
    CHANGE_STATUS,
    CHANGE_EXECUTOR,
    APPLICATION_COMPLETE
}
