package ru.toppink.notificationservice;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.common.securities.SecurityService;
import java.util.Set;

@RestController
@RequiredArgsConstructor
public class SimpleController {

    private final SecurityService securityService;

    @GetMapping("/test")
    public Set<String> test() {
        return securityService.getRoles();
    }
}
