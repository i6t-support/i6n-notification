package ru.toppink.notificationservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.notificationservice.dto.NotificationDto;
import ru.toppink.notificationservice.service.NotificationService;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController("/api/v1")
public class NotificationController {

    private final NotificationService notificationService;
    private final SecurityService securityService;

    @GetMapping("/find")
    @PreAuthorize("isAuthenticated() and hasRole('TUZ')")
    public Page<NotificationDto> getNotificationsByLogin(@RequestParam String login, Pageable pageable) {
        return notificationService.getByLogin(login, pageable);
    }

    @GetMapping("/self")
    @PreAuthorize("isAuthenticated()")
    public Page<NotificationDto> getMyNotifications(Pageable pageable) {
        return notificationService.getByLogin(securityService.getLogin(), pageable);
    }

    @PutMapping("/mark")
    @PreAuthorize("isAuthenticated()")
    public void markAsRead(@RequestBody List<UUID> notifications){
        String login = securityService.getLogin();
        notificationService.markIsRead(login, notifications);
    }
}
