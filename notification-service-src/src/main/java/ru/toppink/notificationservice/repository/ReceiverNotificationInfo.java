package ru.toppink.notificationservice.repository;

import org.hibernate.annotations.Type;
import ru.toppink.notificationservice.model.NotificationType;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.UUID;

public interface ReceiverNotificationInfo {

    String getId();
    String getApplicationId();
    String getExecutor();
    Timestamp getCreatedAt();
    NotificationType getType();
    boolean getIsRead();

    default OffsetDateTime convertDateTime(Timestamp timestamp) {
        if (timestamp != null) {
            OffsetDateTime odt = OffsetDateTime.ofInstant(Instant.ofEpochMilli(timestamp.getTime()), ZoneId.of("Europe/Moscow"));
            return OffsetDateTime.ofInstant(odt.toInstant(), ZoneId.of("Europe/Moscow"));
        } else {
            return null;
        }
    }
}
