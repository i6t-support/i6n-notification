package ru.toppink.notificationservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.toppink.notificationservice.model.Notification;
import java.util.List;
import java.util.UUID;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, UUID> {

    @Query(value = "" +
            "select CAST(r.notification_id as varchar) AS ID, n.application_id AS APPLICATIONID," +
            "       n.executor AS EXECUTOR, n.created_at AS CREATEDAT," +
            "       n.type AS TYPE, r.is_read AS ISREAD " +
            " from receiver r " +
            "          inner join notification n on r.notification_id = n.id " +
            " where r.login = :login " +
            " order by r.is_read asc, n.created_at desc",
            nativeQuery = true
    )
    Page<ReceiverNotificationInfo> getNotificationByLogin(@Param("login") String login, Pageable pageable);


    @Modifying
    @Query("update Receiver r" +
            " set r.isRead = true " +
            " where r.notification.id in :notifications" +
            " and r.login = :login")
    void updateIsReadByLogin(@Param("login") String login,
                             @Param("notifications") List<UUID> notifications);
}
