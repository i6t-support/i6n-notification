package ru.toppink.notificationservice.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.toppink.common.securities.dao.User;
import ru.toppink.common.securities.UserFactory;
import ru.toppink.common.securities.dao.UserRole;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor
public class CommonConfig {
    
    @Bean
    @Primary
    public UserFactory userFactory() {
        return login -> {
            User user = new User();
            user.setLogin(login);
            return user;
        };
    }
}
