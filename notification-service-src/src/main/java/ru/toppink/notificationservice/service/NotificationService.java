package ru.toppink.notificationservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.toppink.notificationservice.dto.NotificationDto;
import ru.toppink.notificationservice.model.NotificationType;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface NotificationService {

    void create(String applicationId, String executor, NotificationType type, Set<String> receivers);

    Page<NotificationDto> getByLogin(String login, Pageable pageable);

    void markIsRead(String login, List<UUID> notifications);
}
