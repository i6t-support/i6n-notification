package ru.toppink.notificationservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.notificationservice.dto.NotificationDto;
import ru.toppink.notificationservice.model.Notification;
import ru.toppink.notificationservice.model.NotificationType;
import ru.toppink.notificationservice.model.Receiver;
import ru.toppink.notificationservice.repository.NotificationRepository;
import ru.toppink.notificationservice.repository.ReceiverNotificationInfo;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository repository;
    private final SecurityService securityService;

    @Override
    public void create(String applicationId, String executor, NotificationType type, Set<String> receiverLogins) {
        log.debug("Creating notification - start: applicationId={}, executor={}, type={}, receivers={}",
                applicationId, executor, type, receiverLogins);


        Notification notification = new Notification();
        notification.setApplicationId(applicationId);
        notification.setExecutor(executor);
        notification.setType(type);
        notification.setCreatedAt(OffsetDateTime.now());


        Set<Receiver> receivers = receiverLogins.stream()
                .map(receiverLogin -> getReceiverFromLogin(notification, receiverLogin))
                .collect(Collectors.toSet());

        notification.setReceivers(receivers);

        Notification saved = repository.save(notification);

        log.debug("Creating notification - end: {}", saved);
    }

    @Override
    public Page<NotificationDto> getByLogin(String login, Pageable pageable) {
        log.debug("Getting notification page by login - start by user {}: login={}, pageable={}",
                securityService.getLogin(), login, pageable);

        Page<ReceiverNotificationInfo> notificationPage = repository.getNotificationByLogin(login, pageable);

        List<NotificationDto> content = notificationPage.stream().map(notification ->
                NotificationDto.builder()
                        .id(UUID.fromString(notification.getId()))
                        .applicationId((notification.getApplicationId()))
                        .executor(notification.getExecutor())
                        .type(notification.getType())
                        .createdAt(notification.convertDateTime(notification.getCreatedAt()))
                        .isRead(notification.getIsRead())
                        .build()
        )
                .collect(Collectors.toList());

        Page<NotificationDto> notificationDtoPage = new PageImpl<>(
                content, notificationPage.getPageable(), notificationPage.getTotalElements());

        log.debug("Getting notification page by login - end: result={}", notificationDtoPage);
        return notificationDtoPage;
    }

    @Override
    @Transactional
    public void markIsRead(String login, List<UUID> notifications) {
        log.debug("Marking notifications as read - start: login={}, notifications={}", login, notifications);

        repository.updateIsReadByLogin(login, notifications);
        log.debug("Marking notifications as read - end");
    }

    private Receiver getReceiverFromLogin(Notification notification, String login) {
        Receiver receiver = new Receiver();
        receiver.setLogin(login);
        receiver.setNotification(notification);
        receiver.setRead(false);
        return receiver;
    }
}
