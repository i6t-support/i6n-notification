package ru.toppink.notificationservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.procedure.spi.ParameterRegistrationImplementor;
import org.springframework.format.annotation.DateTimeFormat;
import ru.toppink.notificationservice.model.NotificationType;
import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotificationDto {

    private UUID id;
    private String applicationId;
    private String executor;
    private OffsetDateTime createdAt;
    private NotificationType type;
    private boolean isRead;
}
