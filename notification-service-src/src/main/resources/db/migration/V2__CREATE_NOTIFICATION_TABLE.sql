create table notification(
    id uuid not null,
    application_id varchar(63) not null,
    executor varchar(63),
    created_at timestamptz not null,
    notification varchar(63) not null,
    PRIMARY KEY(id)
);