create table receiver(
    id uuid not null,
    notification_id uuid not null,
    login varchar(63) not null,
    is_read boolean not null default false,
    PRIMARY KEY(id)
);

alter table receiver
add constraint notification_receiver_fk
	foreign key (notification_id)
	references notification;